#include <stdio.h>
#include <string.h>

#include <psp2/kernel/threadmgr.h>
#include <psp2/ctrl.h>

#include <vita2d.h>

void colorPick() {
	vita2d_pgf *pgf = vita2d_load_default_pgf();
	int color[4] = {0, 0, 0, 255};
	float rgbaX = 40;
	SceCtrlData pad;
	int curV = 0;

	memset(&pad, 0, sizeof(pad));

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (pad.buttons & SCE_CTRL_RIGHT || pad.buttons & SCE_CTRL_LEFT) {
			int isPadRight = pad.buttons & SCE_CTRL_RIGHT ? 1:0;

			curV = isPadRight ?
			       (curV+1 > 3 ? 0:++curV) :
			       (curV-1 < 0 ? 3:--curV);

			sceKernelDelayThread(150000);
		} else if (pad.buttons & SCE_CTRL_UP || pad.buttons & SCE_CTRL_DOWN) {
			int isPadUp = pad.buttons & SCE_CTRL_UP ? 1:0;

			color[curV] = isPadUp ?
			              (color[curV]+1 > 255 ? 0:color[curV]+1) :
			              (color[curV]-1 < 0 ? 255:color[curV]-1);

			sceKernelDelayThread(120000);
		} else if (pad.buttons & SCE_CTRL_LTRIGGER || pad.buttons & SCE_CTRL_RTRIGGER) {
			int isL = pad.buttons & SCE_CTRL_LTRIGGER ? 1:0;

			color[curV] = !isL ?
			              (color[curV]+1 > 255 ? 0:color[curV]+1) :
			              (color[curV]-1 < 0 ? 255:color[curV]-1);
		}

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_draw_rectangle(40, 40, 200, 200, RGBA8(color[0], color[1], color[2], color[3]));

		for (int i=0; i<4; ++i) {
			unsigned col = curV == i ? RGBA8(0,255,0,240):RGBA8(255,255,255,240);

			vita2d_pgf_draw_textf(pgf, rgbaX, 280, col, 1.3f, "%d", color[i]);
			rgbaX += 70;
		}
		rgbaX = 40;

		vita2d_end_drawing();
		vita2d_swap_buffers();
	}
}

int main() {
	vita2d_init();
	colorPick();
	vita2d_fini();
}
