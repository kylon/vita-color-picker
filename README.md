## Vita Color Picker  

### Usage  
Up / Down - increase / decrease  

Left / Right - select color (R, G, B, A)  

L / R - fast decrease / fast increase
